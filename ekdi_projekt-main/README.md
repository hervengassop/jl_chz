# ekdi_projekt
Repository für unser EKdI-Projekt

# Was ist supabase?
https://flaming.codes/de/posts/supabase-backend-as-a-service-firebase-alternative

https://supabase.com/

# Installation
## Windows
- Git
- Docker (https://docs.docker.com/engine/install/)
- Supabase cli (https://supabase.com/docs/reference/cli/installing-and-updating)

## Einleitung zur localen Entwicklung
https://supabase.com/docs/guides/local-development

# JavaScript-Projekt mit Supabase
## Installation des Frameworks
https://supabase.com/docs/reference/javascript/installing

## Initialisierug der Verbindung
https://supabase.com/docs/reference/javascript/initializing

