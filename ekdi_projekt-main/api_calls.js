import { createClient } from "https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm";
import Appointment from "./appointment.js";
const supabase = createClient(
    "https://ubuczciocgvxfmsoftid.supabase.co",
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVidWN6Y2lvY2d2eGZtc29mdGlkIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTM1Nzc0NTYsImV4cCI6MTk2OTE1MzQ1Nn0.16YHv2lrz1b3NEgO6Bf3LCInpG_A3-B2bKZ6OH1AbW4"
);
console.log("Supabase Instance: ", supabase);

async function getAllAppointments() {
    const data = await supabase.from("appointments").select();
    return data.data;
}

export function createAppointmentElement(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
    return new Appointment(
        first_name,
        last_name, 
        phone_number,
        e_mail_address,
        street,
        post_code,
        city,
        appointment_reason,
        notes,
        date,
        appointment_length,
        appointment_time
    );
}

async function getSpecificAvailableAppointment(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
    let createdAppointment = createAppointmentElement(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time);
    const data = await supabase.from('appointments').select().match({
        date: createdAppointment.date,
        appointment_time: createdAppointment.appointment_time,
    });
    return data.data;
}

async function getSpecificBookedAppointment(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
    let createdAppointment = createAppointmentElement(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time);
    let data = await supabase.from("booked_appointments").select().match({
        first_name: createdAppointment.first_name,
        last_name: createdAppointment.last_name,
        date: createdAppointment.date,
        appointment_length: createdAppointment.appointment_length,
    });
    return data.data;
}

async function deleteAppointment(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
    let createdAppointment = createAppointmentElement(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time);
    await supabase.from("booked_appointments").delete().match({
        first_name: createdAppointment.first_name,
        last_name: createdAppointment.last_name,
        phone_number: createdAppointment.phone_number,
        e_mail_address: createdAppointment.e_mail_address,
        street: createdAppointment.street,
        post_code: createdAppointment.post_code,
        city: createdAppointment.city,
        appointment_reason: createdAppointment.appointment_reason,
        notes: createdAppointment.notes,
        date: createdAppointment.date,
        appointment_length: createdAppointment.appointment_length,
    });

    //TODO: AppointmentTime an Appointment anpassen
    await supabase.from('appointments').insert(
        {
            date: createdAppointment.date,
            appointment_time: createdAppointment.date
        }
    )
}

//await getSpecificAppointment("test", "test", "test", "test", "test", "test", "test", "test", "test", "test", "test");

export async function createAppointment(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
    let createdAppointment = createAppointmentElement(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time);
    let availableAppointments = await getSpecificAvailableAppointment(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time);
    if (availableAppointments.length > 0) {
        await supabase.from("booked_appointments").insert([
            {
                first_name: createdAppointment.first_name,
                last_name: createdAppointment.last_name,
                phone_number: createdAppointment.phone_number,
                e_mail_address: createdAppointment.e_mail_address,
                street: createdAppointment.street,
                post_code: createdAppointment.post_code,
                city: createdAppointment.city,
                appointment_reason: createdAppointment.appointment_reason,
                notes: createdAppointment.notes,
                date: createdAppointment.date,
                appointment_length: createdAppointment.appointment_length,
            },
        ]);
        await supabase.from("appointments").delete().match({
            date: createdAppointment.date,
        });
        console.log("Appointment booked");
        return true;
    } else {
        console.log("No available Appointments");
        return false;
    }
}

//await createAppointment("test", "test", "test", "test", "test", "test", "test", "test", "test", "test", "test", "test");
//await deleteAppointment("test", "test", "test", "test", "test", "test", "test", "test", "test", "test", "test", "test");