export default class Appointment {
    constructor(first_name, last_name, phone_number, e_mail_address, street, post_code, city, appointment_reason, notes, date, appointment_length, appointment_time) {
        this.first_name = first_name;
        this.last_name = last_name; 
        this.phone_number = phone_number;
        this.e_mail_address = e_mail_address;
        this.street = street;
        this.post_code = post_code;
        this.city = city;
        this.appointment_reason = appointment_reason;
        this.notes = notes;
        this.date = date;
        this.appointment_length = appointment_length;
        this.appointment_time = appointment_time;
    }
}
