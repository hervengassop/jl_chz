
function handleOnClick(){
    console.log("Hi!")

    const first_name = document.getElementById("vorname").value;
    const last_name = document.getElementById("nachname").value;
    const tel = document.getElementById("tel").value;
    const email = document.getElementById("email").value;
    const strasse = document.getElementById("strasse").value;
    const plz = document.getElementById("plz").value;
    const ort = document.getElementById("ort").value;
    const termingrund = document.getElementById("termingrund").value;
    const hinweise = document.getElementById("hinweise").value;
    const datum = document.getElementById("datum").value;
    const uhrzeit = document.getElementById("uhrzeit").value;
    let dauer;
    var radios = document.getElementsByName('dauer');
    
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            dauer = radios[i].value;
            // only one radio can be logically checked, don't check the rest
            break;
        }
    }

    let appointment = createAppointmentElement();
    var status = createAppointment(appointment);
    // Status der Reservierung aufzeigen
    reservierung_status(appointment, status);  
} 

function display_fetched_data(appointment_param) { 
    var appointment = appointment_param;
    document.getElementById("id_vorname").innerText = appointment.first_name;
    document.getElementById("id_nachname").innerText = appointment.last_name;
    document.getElementById("id_telefonnummer").innerText = appointment.phone_number;
    document.getElementById("id_email").innerText = appointment.e_mail_address;
    document.getElementById("id_strasse").innerText = appointment.street;
    document.getElementById("id_plz").innerText = appointment.post_code;
    document.getElementById("id_ort").innerText = appointment.city;
    document.getElementById("id_termingrund").innerText = appointment.appointment_reason;
    document.getElementById("id_hinweise").innerText = appointment.notes;
    document.getElementById("id_datum").innerText = appointment.date;
    document.getElementById("id_dauer").innerText = appointment.appointment_length;
    document.getElementById("id_uhrzeit").innerText = appointment.appointment_time;
}

function reservierung_status(appointment_param, status) { 
    // Die Seite Bestaetigung.html in einem neuen Tab öffnen
    window.open("Bestaetigung.html", "_blank");
    
    display_fetched_data(appointment_param);
    if(status){
        document.getElementById("bookingStatusOk").style.display = "block";
        document.getElementById("bookingStatusNok").style.display = "none";
    }
    else{
        document.getElementById("bookingStatusOk").style.display = "none";
        document.getElementById("bookingStatusNok").style.display = "block";
    }
    
  }