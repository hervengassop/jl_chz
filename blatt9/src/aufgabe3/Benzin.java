package aufgabe3;

import utils.CustomArray;

import java.util.Arrays;
import java.util.Random;

public class Benzin {
    final static int rows = 3;
    final static int colums = 7;
    final static Random rnd = new Random();

    /**
     * Methode erzeugt zufällige Werte, welche jeweils einen Benzinpreis entspricht.
     * Die Werte sind allesamt positiv und innerhalb der angegebenen Grenze.
     * Ferner sind sie so angeordnet, dass die Spalten die Wochentage und die Zeilen die
     * Messzeitpunkte repräsentieren. Das Array ist demzufolge * 3 x 7 lang. Zum Beispiel:
     * 1.08 1.51 1.26 1.39 1.88
     * 1.73 1.94 1.25 1.05 1.21
     * 1.49 1.79 1.15 1.53
     * Dabei entspricht beispielsweise der Wert dagegen am Sonntag Abend.
     *
     * @param min Die Untergrenze (der kleinstmögliche Preis)
     * @param max Die Obergrenze (der größtmögliche Preis)
     * @return Ein zweidimensionales Array mit zufällig generierten Benzinpreise
     */
    public static double[][] generiereWerte(double min, double max) {
        if (min <= 0 || max <= 0) {
            System.err.println("Die folgende Bedingungen sollen gelten: min > 0; max >0");
            return new double[0][0];
        } else if (max <= min) {
            System.err.println("Die folgende Bedingung soll gelten: min < max");
            return new double[0][0];
        }

        final double[][] arr = new double[rows][colums];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colums; j++) {
                arr[i][j] = rnd.nextDouble() * ((max - min + 1) + min);
            }
        }

        return arr;
    }

    /**
     * Methode schreibt alle Benzinpreise samt den Durchschnittswerten angeordnet
     * als Tabelle auf die * Konsole aus. Die Durchschnittspreise werden sowohl
     * spalten- als auch zeilenweise mit ausgegeben.
     * Zum Beispiel:
     * <p>
     * Abend 1.49 1.79 1.15 1.53 1.04 1.19 1.48 1.38
     * Durchschnitt 1.43 1.75 1.22 1.32 1.38 1.21 1.58 1.41
     * </p>
     *
     * @param preise Das Array mit den Benzinpreisen
     */
    public static void printTabelle(double[][] preise) {
        System.out.println("              Montag  Dienstag  Mittwoch  Donnerstag  Freitag Samstag  Sonntag Durchschnitt");

        CustomArray vormittag = new CustomArray(colums);
        CustomArray nachmittag = new CustomArray(colums);
        CustomArray abend = new CustomArray(colums);

        double rows_mean = 0;
        double rows_mean_arr[] = new double[rows];
        double columns_mean = 0;

        for (int i = 0; i < rows; i++) {
            String row_values = "";

            for (int j = 0; j < colums; j++) {
                int padding_size = 0;

                switch (j) {
                    case 0:
                    case 4:
                    case 6:
                        padding_size = 4;
                        break;
                    case 1:
                    case 2:
                        padding_size = 6;
                        break;
                    case 3:
                        padding_size = 8;
                        break;
                    case 5:
                        padding_size = 5;
                        break;
                }
                row_values = row_values.concat(String.format("%.2f", preise[i][j]) + buildStringWithSameValue(" ", padding_size));
                rows_mean += preise[i][j];
            }

            rows_mean /= colums;
            rows_mean_arr[i] = rows_mean;

            row_values = row_values.concat(String.format("%.2f", rows_mean));

            String prefix = " ";

            switch (i) {
                case 0:
                    // Vormittag
                    prefix = prefix.concat("Vormittag    ");
                    break;
                case 1:
                    // Nachmittag
                    prefix = prefix.concat("Nachmittag   ");
                    break;
                case 2:
                    // Abend
                    prefix = prefix.concat("Abend        ");
            }

            System.out.println(prefix + row_values);
        }

        String columns_values = "";

        for (int j = 0; j < colums; j++) {
            for (int i = 0; i < rows; i++) {
                columns_mean += preise[i][j];
            }

            int padding_size = 0;

            switch (j) {
                case 0:
                    padding_size = 4;
                    break;
                case 1:
                    padding_size = 6;
                    break;
                case 2:
                    padding_size = 6;
                    break;
                case 3:
                    padding_size = 8;
                    break;
                case 4:
                    padding_size = 4;
                    break;
                case 5:
                    padding_size = 5;
                    break;
                case 6:
                    padding_size = 4;
                    break;
            }

            columns_mean /= rows;

            columns_values = columns_values.concat(String.format("%.2f",
                    columns_mean) + buildStringWithSameValue(" ", padding_size));

        }

        double rows_mean_arr_mean = 0;

        for (int i = 0; i < rows; i++) {
            rows_mean_arr_mean += rows_mean_arr[i];
        }

        rows_mean_arr_mean /= rows;

        System.out.println("Durchschnitt  " + columns_values +
                String.format("%.2f", rows_mean_arr_mean));
    }

    /**
     * Methode berechnet den Durchschnittswert über alle Elementen des Arrays.
     * Das Array beinhaltet * die Benzinpreise von Montag bis Sonntag an einem
     * bestimmten Messzeitpunkt (z.B. Vormittag).
     * Beispiel: preise = {1.08, 1.51, 1.26, 1.39, 1.88, 1.34, 1.45} | Mittelwert = 1.42
     *
     * @param preise Das Array mit den Benzinpreisen, deren Mittelwert zu berechnen ist.
     * @return Der ermittelte Mittelwert
     */
    public static double berechneWochendurchschnittspreisAmMesszeitpunkt(double[] preise) {
        if (preise != null && preise.length > 0) {
            double mean = 0;

            for (int i = 0; i < preise.length; i++) {
                mean += preise[i];
            }

            mean /= preise.length;
            System.out.println(Arrays.toString(preise) + " | Mittelwert = " +
                    String.format("%.2f", mean));

            return mean;
        } else {
            System.err.println("Der Parameter preise ist null oder leer");
            return 0;
        }
    }

    /**
     * Methode berechnet den Durchschnittswert der jeweiligen Spalten des Arrays.
     * Sie rechnet somit den * Benzindurchschnittspreis über alle drei Messzeitpunkten
     * jeweils an einem bestimmten Wochentag.
     * Beispiel:
     * <p>
     * <p>
     * <p>
     * * @return Ein neues Array mit allen ermittelten Mittelwerten.
     */
    public static double[] berechneDurchschnittspreisProWochentag(double[][] preise) {

        if (preise != null && preise.length > 0) {
            double result[] = new double[preise.length];

            for (int i = 0; i < preise.length; i++) {
                double mean = 0;

                for (int j = 0; j < preise[i].length; j++) {
                    mean += preise[j][i];
                }

                result[i] = mean / preise[i].length;
            }

            return result;
        } else {
            System.err.println("Der Parameter preise ist null oder leer");
            return new double[0];
        }
    }

    /**
     * Methode berechnet den Durchschnittswert über alle Elementen des Arrays.
     *
     * @param preise Das Array mit den Benzinpreisen, deren Mittelwert zu berechnen ist. * @return Der Mittelwert über alle Elementen
     */
    public static double berechneDurchschnitt(double[][] preise) {
        if (preise != null && preise.length > 0) {
            double mean = 0;
            int rows = preise.length;
            int columns = preise[0].length;

            for (int i = 0; i < preise.length; i++) {
                for (int j = 0; j < preise[i].length; j++) {
                    mean += preise[i][j];
                }
            }

            // Eine n*m-Matrix hat insgesamt n*m Elemente
            return mean / (rows * columns);
        } else {
            System.err.println("Der Parameter preise ist null oder leer");
            return 0;
        }
    }

    private static String buildStringWithSameValue(String value, int size) {
        String result = "";

        if (size > 0 && value != null) {
            for (int i = 0; i < size; i++) {
                result = result.concat(value);
            }
        }

        return result;
    }
}
