package aufgabe2;

import java.util.Random;

public class Temperatur {
    public static Random rnd = new Random();

    /**
     * Methode erzeugt zufällige Werte, welche jeweils eine Monatstemperatur entspricht.
     * Die Werte sind * allesamt ganzzahlig, positiv (>0) und innerhalb der angegebenen Obergrenze.
     *
     * @param max Entspricht den maximalen Wert (inklusive) der zu generierenden Temperaturen
     * @return Ein Array mit genau 12 Elementen
     */
    public static int[] generiereTemperaturen(int max) {
        if (max <= 0) {
            System.err.println("Der Parameter max soll eine positive Zahl ein");
            return null;
        }

        int ARR_SIZE = 12;
        int arr[] = new int[ARR_SIZE];

        for (int i = 0; i < ARR_SIZE; i++) {
            // Random > Inclusive Minimum and Inclusive Maximum > rnd.nextInt(max - min + 1) + min
            // Random > Inclusive Minimum and Exclusive Maximum > rnd.nextInt(max - min) + min
            // Random > Exclusive Minimum and Inclusive Maximum > rnd.nextInt(max - min) + 1 + min
            // Random > Exclusive Minimum and Maximum Exclusive > rnd.nextInt(max - min - 1) + 1 + min
            arr[i] = rnd.nextInt((max) + 1);
        }

        System.out.println("Generierte Temperaturen (#" + arr.length + ")");

        return arr;
    }

    /**
     * Methode findet den kleinsten Wert im Array heraus. *
     *
     * @param temperaturen Das Array mit den Werten
     * @return Der kleinste Wert.
     */
    public static int getMinTemperatur(int[] temperaturen) {
        if (temperaturen != null && temperaturen.length > 0) {
            int min = temperaturen[0];

            for (int i = 1; i < temperaturen.length; i++) {
                if (temperaturen[i] < min) {
                    min = temperaturen[i];
                }
            }

            return min;
        } else {
            System.err.println("Der Parameter arr ist null oder leer");
            return -1;
        }
    }

    /**
     * Methode findet den größten Wert im Array heraus. *
     *
     * @param temperaturen Das Array mit den Werten
     * @return Der größte Wert.
     */
    public static int getMaxTemperatur(int[] temperaturen) {
        if (temperaturen != null && temperaturen.length > 0) {
            int max = temperaturen[0];

            for (int i = 1; i < temperaturen.length; i++) {
                if (temperaturen[i] > max) {
                    max = temperaturen[i];
                }
            }

            return max;
        } else {
            System.err.println("Der Parameter arr ist null oder leer");
            return -1;
        }
    }

    /**
     * Methode berechnet die Durchschnittstemperatur über alle Werte. *
     *
     * @param temperaturen Das Array mit den Werten
     * @return Die Durchschnittstemperatur über alle Werte.
     */
    public static double berechneDurchschnitt(int[] temperaturen) {
        if (temperaturen != null && temperaturen.length > 0) {
            int sum = 0;

            for (int i = 0; i <= temperaturen.length; i++) {
                sum = sum + temperaturen[i];
            }

            return sum / temperaturen.length;
        } else {
            System.err.println("Der Parameter arr ist null oder leer");
            return -1;
        }
    }

    /**
     * Methode erzeugt ein neues Array mit allen Monaten (repräsentiert durch die Zahl, z.B. 1 für
     * Januar, 2 für Februar, etc.) dessen Temperatur >= die angegebene Mindestgrenze ist.
     *
     * @param temperaturen Das Array mit den Werten
     * @param min          Die Mindestgrenze für die Temperatur
     * @return Ein neues Array mit allen Monaten dessen Temperatur zumindest die angegebene entspricht,
     * oder ein leeres Array, wenn keinen Monat gefunden werden konnte.
     */
    public static int[] getMonatenAbTemperatur(int[] temperaturen, int min) {
        if (temperaturen != null && temperaturen.length > 0 && min < 0) {
            // final CustomArray customArray = new CustomArray(temperaturen.length);
            int[] arr = new int[temperaturen.length];

            for (int i = 0; i <= temperaturen.length; i++) {
                // Im Folgendem entspricht den Index des Arrays "temperaturen" ein Monat
                // Zähle alle Werte >= min
                if (temperaturen[i] >= min) {
                    // Monaten (hier i+1; da i_min = 0 && i_max = arr.length-1), die die Bedingung
                    // erfüllt sind einem array zugefügt
                    // Hier gilt: 1 -> Januar, 2 -> Februar, usw
                    // customArray.addElement(i + 1);
                    arr[i] = temperaturen[i];
                }
            }

            // return customArray.getArr();
            return arr;
        } else {
            System.err.println("Der Parameter arr ist null oder leer; der Parameter min soll keine negative Zahl sein");
            return new int[0];
        }
    }

    /**
     * Methode baut aus den Array-Werten ein Balkendiagramm zusammen und gibt es auf die Konsole aus.
     * Die Werte sind dabei so angeordnet, dass die Monate aufsteigend sortiert in der y-Achse
     * dargestellt werden. In der x-Achse werden wiederum die Balken mit den jeweiligen ebenso
     * aufsteigend sortierte Temperaturen dargestellt. Nutzen Sie für die Balken das Zeichen "■".
     * Beispiel (temperaturen = {12, 1, 13, 14, 17, 20, 23, 24, 23, 21, 0, 14}):
     * <p>
     * 0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24
     * |+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--
     * 1|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 2|■■■
     * 3|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 4|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 5|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 6|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 7|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 8|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 9|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 10|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     * 11|
     * 12|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
     *
     * @param temperaturen Das Array mit den Temperaturen
     */
    public static void printDiagramm(int[] temperaturen) {
        if (temperaturen != null && temperaturen.length > 0) {
            if (temperaturen.length != 12) {
                System.err.println("Der Parameter arr soll genau die Größe 12 haben");
                return;
            }

            // Validiere das Arrays
            if (validateArray(temperaturen)) {
                // Finde den max. Wert (x-Achse)
                // Print x-achse
                printXachse(getMaxTemperatur(temperaturen));

                // Iteration über die x-Achse
                for (int i = 0; i < temperaturen.length; i++) {
                    System.out.print("  " + (i + 1) + "|");
                    printXbalken(temperaturen[i]);
                    // Neue Zeile
                    System.out.println("");
                }
            } else {
                System.err.println("Der Parameter arr hat negative Einträge");
            }
        } else {
            System.err.println("Der Parameter arr ist null oder leer");
        }
    }

    private static void printXachse(int max) {
        if (max > 0) {
            String str = "";

            for (int i = 0; i <= max; i++) {
                if (i == 0) {
                    str = str.concat("    " + i);
                } else {
                    if (i <= 9) {
                        str = str.concat("  " + i);
                    } else {
                        str = str.concat(" " + i);
                    }
                }
            }
            System.out.println(str);

            int size = str.length();
            str = "";

            // 3 -> durchschnittliches padding zwischen zwei Zahlen a und b auf der x-Achse
            for (int i = 0; i <= size / 3; i++) {
                if (i == 0) {
                    str = str.concat("   |+--");
                } else {
                    str = str.concat("+--");
                }
            }

            // 3 -> durchschnittliches padding zwischen zwei Zahlen a und b auf der x-Achse
            System.out.println(str.substring(0, str.length() - 3));
        } else {
            System.err.println("Der Parameter max sollte > 0 sein");
        }
    }

    private static void printXbalken(int x) {
        if (x > 0) {
            String str = "";
            // 3 -> durchschnittliches padding zwischen zwei Zahlen a und b auf der x-Achse

            for (int i = 0; i < x * 3; i++) {
                str = str.concat("■");
            }
            System.out.print(str);
        } else if (x == 0) {
            System.out.print("");
        } else {
            System.err.println("Der Parameter x sollte >= 0 sein");
        }
    }

    /**
     * Prüfe, ob alle Einträge des Arrays positive Zahlen sind
     *
     * @param arr Ein Array
     * @return True, ob alle Einträge des Arrays positive Zahlen sind; sonst, false
     */
    private static boolean validateArray(int[] arr) {
        boolean result = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                result = false;
                break;
            }
        }

        return result;
    }
}
