package aufgabe1;

import utils.CustomArray;

public class Eratosthenes {

    /**
     * Methode ermittelt alle Primzahlen bis zur angegebenen Obergrenze (inklusive). Als
     * Berechnungsgrundlage dient der sogenannte "Sieb des Eratosthenes" (siehe
     * https://de.wikipedia.org/wiki/Sieb_des_Eratosthenes). Negative Werte für die Obergrenze sind wie * positive zu behandeln. Beispiel:
     * Obergrenze = 11 | Primzahlen = [2, 3, 5, 7, 11]
     * Obergrenze = -9 | Primzahlen = [2, 3, 5, 7]
     *
     * @param obergrenze Die Obergrenze bis zu welcher Zahl die Primzahlen ermittelt werden sollen
     * @return ein Array mit allen Primzahlen bis zur Obergrenze, oder ein leeres Array, wenn keine
     */
    public static int[] primezahlen(int obergrenze) {
        if (obergrenze == 0) {
            System.err.println("Der Parameter obergrenze muss ungleich null sein");
            return new int[0];
        }

        if (obergrenze < 0) {
            obergrenze = Math.abs(obergrenze);
        }


        boolean[] gestrichen = init_array_with_bool_value(new boolean[obergrenze + 1], false);
        final CustomArray customArray = new CustomArray(obergrenze);

        for (int i = 2; i <= Math.sqrt(obergrenze); i++) {
            if (!gestrichen[i]) {
                System.out.println(i + " ist eine Primzahl");
                customArray.addElement(i);
                // Vielfachen von i streichen ...
                for (int j = i * i; j <= obergrenze; j = j + i) {
                    gestrichen[j] = true;
                }
            }
        }

        for (int i = new Double(Math.sqrt(obergrenze)).intValue() + 1; i <= obergrenze; i++) {
            if (!gestrichen[i]) {
                System.out.println(i + " ist eine Primzahl");
                customArray.addElement(i);
            }
        }

        // Alle unmarkierten Zahlen sind Primzahlen
        // Bemerkung: das Array-Ergebnis ist mit
        return customArray.getArr();
    }

    private static boolean[] init_array_with_bool_value(boolean[] arr, boolean value) {
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                if (i == 0 || i == 1) {
                    arr[i] = true;
                } else {
                    arr[i] = value;
                }
            }

            return arr;
        } else {
            System.err.println("Der Parameter arr ist null oder leer");
            return null;
        }
    }
}
