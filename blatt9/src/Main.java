import aufgabe1.Eratosthenes;
import aufgabe2.Temperatur;
import aufgabe3.Benzin;
import aufgabe4.ArrayManipulation;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(Eratosthenes.primezahlen(13)));
        System.out.println(Arrays.toString(Temperatur.generiereTemperaturen(24)));
        Temperatur.printDiagramm(new int[]{9, 1, 13, 14, 17, 20, 23, 24, 23, 21, 0, 14});

        Benzin.printTabelle(Benzin.generiereWerte(0.25, 1.22));

        System.out.println(Arrays.toString(ArrayManipulation.groesseAendern(
                new int[]{9, 1, 13, 14, 17}, 5)));

        System.out.println(Arrays.toString(ArrayManipulation.einfuegen(
                2,
                new int[]{9, 1, 13},
                1, 2)));


        System.out.println(Arrays.toString(ArrayManipulation.entfernen(
                new int[]{9, 1, 13},
                0,
                3
        )));

        //TODO jUnit tests
    }
}