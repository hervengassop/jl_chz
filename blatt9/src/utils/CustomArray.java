package utils;

/**
 * Eine Klasse, um die Datenstruktur nur mit arrays zu verwalten.
 * Es ginge mit ArraysList einfacher ...
 */
public class CustomArray {
    private int currentIndex = 0;
    private int size;
    private int[] arr;

    public CustomArray(int size) {
        this.size = size;
        this.arr = new int[this.size];
    }

    public void addElement(int element) {
        this.arr[this.currentIndex] = element;
        this.currentIndex++;
    }

    public int[] getArr() {
        int[] var = new int[this.currentIndex];

        for (int i = 0; i < this.currentIndex; i++) {
            var[i] = this.arr[i];
        }

        return var;
    }
}