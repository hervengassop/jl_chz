package aufgabe4;

public class ArrayManipulation {
    /**
     * Erstellt ein int-Array mit der angegebenen Größe. Soweit möglich werden
     * die Elemente von ‚array‘ in das erstellte Array kopiert.
     * Nicht belegte Array-Elemente erhalten den Wert 0.
     *
     * @param array       Array von int-Werten, dessen Größe geändert werden soll.
     * @param neueGroesse die neue Größe des Arrays.
     * @return Ein int-Array mit der neuen Größe oder null, falls ‚array‘ null ist. Es gilt dabei, dass
     */
    public static int[] groesseAendern(int[] array, int neueGroesse) {
        if (array != null && array.length > 0) {
            if (neueGroesse < 0) {
                return array;
            }

            if (neueGroesse >= 0) {
                // Falls die neue Array-Größe 0 ist, wird ein leeres Array ausgegeben
                if (neueGroesse == 0) {
                    return new int[0];
                } else if (neueGroesse == 1) {
                    // Falls die neue Array-Größe 1 ist, wird ein Array
                    // mit dem ersten Element des Ursprünglichen ausgegeben
                    return new int[]{array[0]};
                } else if (neueGroesse <= array.length) {
                    // Die neue Array-Größe ist kleiner als das ursprüngliche Array
                    // Array-Truncating
                    int[] result = new int[neueGroesse];

                    for (int i = 0; i < neueGroesse; i++) {
                        result[i] = array[i];
                    }
                    // 1 2 3  n=3

                    return result;
                }
                else if (neueGroesse > array.length) {
                    // Die neue Array-Größe ist größer als das ursprüngliche Array
                    // Array-Extension
                    int[] result = new int[neueGroesse];

                    for (int i = 0; i < neueGroesse; i++) {
                        if (i < array.length) {
                            result[i] = array[i];
                        } else {
                            result[i] = 0;
                        }
                    }

                    return result;
                }
            }
        }

        return null;
    }

    /**
     * Erstellt ein neues int-Array mit den ursprünglichen Werten aus ‚array‘ zusammen
     * mit den aus‚ elemente‘, wobei diese neuen Werte ab dem Array-Index ‚position‘
     * eingefügt werden sollen
     * (vorhandene Werte werden dadurch ggf. nach hinten verschoben).
     *
     * @param position Der Index, ab wo die Elemente eingefügt werden sollen.
     * @param array    Das ursprüngliche Array mit int-Werten.
     * @param elemente Die einzufügende int-Werte.
     * @return Ein neues int-Array mit den eingefügten Elementen oder null, wenn ‚array‘ bereits null
     * *
     */
    public static int[] einfuegen(int position, int[] array, int... elemente) {

        if (array == null) {
            return null;
        }

        if (position < 0 || position > array.length || elemente == null) {
            return array;
        }


        if (elemente.length == 0) {
            return array;
        }

        int[] result = new int[array.length + elemente.length];
        // arr = 1  3  4  5       elemente= 3 4
        // 3 4 1 3 4 5
        if (position == 0) {
            // Die Elemente sind am Beginn hinzufügen
            int result_idx = 0;
            while (result_idx < elemente.length) {
                result[result_idx] = elemente[result_idx];
                result_idx++;
            }

            int idx = position;
            while (result_idx < result.length && position < array.length) {
                result[result_idx] = array[idx];
                idx++;
                result_idx++;
            }

            return result;
        }

        // arr = 1  3  4  5       elemente= 3 4
        // 1 3 4 5 3 4
        if (position == array.length) {
            result = new int[array.length + elemente.length];
            // Die Elemente sind am Ende hinzufügen
            int result_idx = 0;
            while (result_idx < array.length) {
                result[result_idx] = array[result_idx];
                result_idx++;
            }

            int idx = 0;
            while (result_idx < result.length && idx < elemente.length) {
                result[result_idx] = elemente[idx];
                idx++;
                result_idx++;
            }

            return result;
        }

        // arr = 1  3  4  5       elemente= 3 4
        // 1  3 4 3  4  5
        if (position > 0 && position < array.length) {

            result = new int[array.length + elemente.length];

            // Elemente der Intervalle [0, position-1] von array[]
            for (int i = 0; i < position; i++) {
                result[i] = array[i];
            }

            // Elemente der Intervalle [0, elemente.length] von elemente[]
            int arr_idx = position;
            int elemente_idx = 0;

            while (arr_idx < position + elemente.length && elemente_idx < elemente.length) {
                result[arr_idx] = elemente[elemente_idx];
                arr_idx++;
                elemente_idx++;
            }

            // Elemente der Intervalle [position + elemente.length, array.length] von array[]
            int idx = position;

            while (arr_idx < result.length && idx < array.length) {
                result[arr_idx] = array[idx];
                idx++;
                arr_idx++;
            }

            return result;
        }

        return null;
    }

    /**
     * Erstellt ein neues int-Array mit den Werten aus ‚array‘, wobei ‚anzahl‘ Elemente ab dem Index
     * ‚position‘ entfernt werden. Der Versuch mehr Elemente zu löschen als vorhanden ist,
     * löscht alle * Elemente ab Index ‚position‘
     *
     * @param array    Das ursprüngliche Array mit int-Werten.
     * @param position Der Index, ab dem die Elemente entfernt werden sollen.
     * @param anzahl   Die Anzahl der zu entfernenden Elemente.
     * @return Ein neues int-Array ohne die entfernten Elemente oder null, wenn ‚array‘ bereits null
     * *
     */
    public static int[] entfernen(int[] array, int position, int anzahl) {
        if (array == null || anzahl < 0) {
            return null;
        }

        if (position < 0 || position > array.length) {
            return array;
        }

        // Keine Elemente sind zu löschen
        if (anzahl == 0) {
            return array;
        }

        // if (anzahl == array.length) {
        //    return array;
        // }
        // arr = 1  3  4  5       position= 1, anzahl=1
        // 1 4 5

        int[] result = new int[array.length - anzahl];
        int result_idx = 0;

        while (result_idx < position) {
            result[result_idx] = array[result_idx];
            result_idx++;
        }

        int idx = position + anzahl;

        while (idx < array.length && result_idx < result.length) {
            result[result_idx] = array[idx];
            idx++;
            result_idx++;
        }

        return result;
    }
}
