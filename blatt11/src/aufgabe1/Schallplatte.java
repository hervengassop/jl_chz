package aufgabe1;

public class Schallplatte {
    private String kuenstlerName;
    private String albumName;
    private short erscheinungsjahr;
    private double preis;
    private int verfuegbare;
    private int totalAusgeliehen;
    private int id;
    private boolean hasId;


    /**
     * Der default leerer Konstruktor
     */
    public Schallplatte() {
        super(); // Aufruf des Konstruktors der Oberklasse Object
        this.kuenstlerName = null;
        this.albumName = null;
        this.erscheinungsjahr = -1;
        this.preis = -1.0;
        this.verfuegbare = -1;
        this.totalAusgeliehen = -1;
        this.hasId = false;
    }

    /**
     * Der Konstruktor initialisiert die Klasse {@link Schallplatte} mit deren Parameter
     * @param kuenstlerName Der Kuenstlername
     * @param albumName Der Albumname
     * @param erscheinungsjahr Das Erscheinungsjahr der Schallplatte
     * @param preis Der verkaufspreis
     * @param verfuegbare Die Anzahl an verfuegbare Schallplatten
     */
    public Schallplatte(String kuenstlerName, String albumName, short erscheinungsjahr, double preis, int verfuegbare) {
        this(); // Aufruf des leeren Konstruktors
        this.kuenstlerName = kuenstlerName;
        this.albumName = albumName;
        this.erscheinungsjahr = erscheinungsjahr;
        this.preis = preis;
        this.verfuegbare = verfuegbare;
    }

    /**
     * Der Konstruktor initialisiert die Klasse {@link Schallplatte} mit deren Parameter aus einer Zeichenkette
     * @param info
     */
    public Schallplatte(String info) {
        this(); // Aufruf des leeren Konstruktors

        if (info != null && info.length() > 0) {
            String [] values = info.trim().split(";");
            if(values.length == 6){
                this.kuenstlerName = values[0].trim();
                this.albumName = values[1].trim();
                this.erscheinungsjahr =  (short) Integer.parseInt(values[2].trim());
                this.preis = Double.parseDouble(values[3].trim());
                this.verfuegbare = Integer.parseInt(values[4].trim());
                this.totalAusgeliehen = Integer.parseInt(values[5].trim());
            }
            else {
                System.out.println("*** ERROR *** Invalid argument");
            }
        }
        else{
            System.out.println("*** ERROR *** Invalid argument");
        }
    }

    /**
     * Die Methode gibt die ID der Schallplatte zueruck
     *
     * @return Die ID der Schallplatte
     */
    public int getId() {
        return this.id;
    }

    /**
     * The Methode setzt einamlig eine ID zu der Schlallplatte
     * @param id
     */
    public void setId(int id) {
        if (!this.hasId) {
            this.id = id;
            this.hasId = true;
        }
        else {
            System.out.println("*** ERROR *** Die ID der Schallplatte wurde schon gesetzt");
        }
    }

    /**
     * Die Methode gibt die Anzahl von ausgeliehenden Schallplatten zueruck
     *
     * @return Die Anzahl von ausgeliehenden Schallplatten
     */
    public int getTotalAusgeliehen() {
        return this.totalAusgeliehen;
    }

    /**
     * Die Methode abstrahiert die Ausleihe einer Schallplatte, soweit welche noch verfuegbar sind.
     *
     * @return True, falls eine Ausleihe erfolgte. False, sonst.
     */
    public boolean ausleihen(){
        if(this.verfuegbare > 0){
            this.totalAusgeliehen++;
            this.verfuegbare--;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Die Methode abstrahiert die Rueckgabe einer Schallplatte,
     */
    public void zurueckgeben(){
        this.verfuegbare++;
    }

    /**
     * The Methode vergleicht und ueberprueft, ob die IDs unique sind
     * @param schallplatte Eine {@link Schallplatte}
     * @return True, falls die IDs unique sind. False sonst.
     */
    public boolean isUnique(Schallplatte schallplatte){
        if(schallplatte != null){
            return this.id != schallplatte.getId();
        }
        else {
            return false;
        }
    }

    /**
     * The Methode vergleicht und ueberprueft, ob die IDs unique sind
     * @param schallplatten Ein Array von {@link Schallplatte}n
     * @return True, falls die IDs unique sind. False sonst.
     */
    public boolean isUnique(Schallplatte ... schallplatten ){
        for (Schallplatte schallplatte: schallplatten){
            if(!this.isUnique(schallplatte)){
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        return "Schallplatte{" +
                "kuenstlerName='" + kuenstlerName + '\'' +
                ", albumName='" + albumName + '\'' +
                ", erscheinungsjahr=" + erscheinungsjahr +
                ", preis=" + preis +
                ", verfuegbare=" + verfuegbare +
                ", totalAusgeliehen=" + totalAusgeliehen +
                ", id=" + id +
                '}';
    }
}