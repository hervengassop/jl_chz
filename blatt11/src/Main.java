import aufgabe1.Schallplatte;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Schallplatte sp1 = new Schallplatte("Nirvana; Nevermind; 1991; 6.99; 5; 7");
        System.out.println(sp1.toString());

        sp1.setId(23);
        System.out.println(sp1.toString());

        sp1.setId(1);
        System.out.println(sp1.toString());

        // Zurueckgeben 1
        sp1.zurueckgeben();
        System.out.println(sp1.toString());

        // Ausleihen 1
        sp1.ausleihen();
        System.out.println(sp1.toString());

        // Ausleihen 2
        sp1.ausleihen();
        System.out.println(sp1.toString());

        // Ausleihen 3
        sp1.ausleihen();
        System.out.println(sp1.toString());
    }
}